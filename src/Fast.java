import java.util.Arrays;
import java.util.HashMap;

public class Fast {
    public static void main(String[] args) {
        String fileName = args[0];
        In reader = new In(fileName);
        int totalPoints = reader.readInt();
        Point[] points;
        // read all points into an array of points
        try {
            points = new Point[totalPoints];
            for (int i = 0; i < totalPoints; i++) {
                points[i] = new Point(reader.readInt(), reader.readInt());
            }
        } finally {
            reader.close();
            reader = null;
        }
        Arrays.sort(points);

        // set canvas
        StdDraw.setCanvasSize(400, 400);
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        StdDraw.show();

        //for each point in points
        Point[] temp = new Point[totalPoints];
        HashMap<Point, Point> connected = new HashMap<Point, Point>();
        System.arraycopy(points, 0, temp, 0, totalPoints);
        for (int i = 0; i < totalPoints; i++) {
            Point p = points[i];
            if (!connected.containsKey(p)) {
                Arrays.sort(temp, p.SLOPE_ORDER);
                p.draw();

//                double[] slopes = new double[totalPoints];
//                for (int j = 0; j < temp.length; j++) {
//                    slopes[j] = p.slopeTo(temp[j]);
//                }

                // loop through all vertices
                for (int j = 1; j < temp.length; j++) {
                    if (j + 1 < temp.length) {
                        double slope1 = p.slopeTo(temp[j]);
                        int v = 1;
                        while (j + v < temp.length && slope1 == p.slopeTo(temp[j + v])) {
                            if (v > 1) {
                                if (connected.containsKey(p)) {
                                    connected.replace(p, temp[j + v]);
                                } else {
                                    connected.put(p, temp[j + v]);
                                }
                            }
                            v++;
                        }
                        j += v - 1;
                    }
                }
            }
        }
        temp = null;
        for (Point key : connected.keySet()) {
            key.drawTo(connected.get(key));
        }
    }
}
