import java.util.Comparator;

public class Point implements Comparable<Point> {

    // compare points by slope
    public final Comparator<Point> SLOPE_ORDER = new BySlope();       // YOUR DEFINITION HERE

    private final int x;                              // x coordinate
    private final int y;                              // y coordinate

    // create the point (x, y)
    public Point(int x, int y) {
        /* DO NOT MODIFY */
        this.x = x;
        this.y = y;
    }

    // plot this point to standard drawing
    public void draw() {
        /* DO NOT MODIFY */
        StdDraw.point(x, y);
    }

    // draw line between this point and that point to standard drawing
    public void drawTo(Point that) {
        /* DO NOT MODIFY */
        StdDraw.line(this.x, this.y, that.x, that.y);
    }

    // slope between this point and that point
    public double slopeTo(Point that) {
        if (that == null) {
            throw new java.lang.NullPointerException();
        }
        if (that.y - this.y == 0 && that.x - this.x == 0) {
            return Double.NEGATIVE_INFINITY;
        }
        else if (that.x - this.x == 0) { // vertical line
            return Double.POSITIVE_INFINITY;
        }
        double slope = (double)(that.y - this.y) / (that.x - this.x);
        return (slope == 0.0) ? 0.0 : slope;
    }

    // is this point lexicographically smaller than that one?
    // comparing y-coordinates and breaking ties by x-coordinates
    public int compareTo(Point that) {
        if (that == null) {
            throw new java.lang.NullPointerException();
        }
        if (this.y < that.y) {
            return -1;
        }
        else if (this.y == that.y && this.x < that.x) {
            return -1;
        }
        else if (this.y == that.y && this.x == that.x) {
            return 0;
        }
        return 1;
    }

    // return string representation of this point
    public String toString() {
        /* DO NOT MODIFY */
        return "(" + x + ", " + y + ")";
    }

    private class BySlope implements Comparator<Point> {

        @Override
        public int compare(Point p1, Point p2) {
            if (p1 == null || p2 == null) {
                throw new java.lang.NullPointerException();
            }
            double p1Slope = Point.this.slopeTo(p1);
            double p2Slope = Point.this.slopeTo(p2);

            if (p1Slope < p2Slope) {
                return -1;
            }
            else if (p1Slope > p2Slope) {
                return 1;
            }
            return p1.compareTo(p2);
        }
    }

    // unit test
    public static void main(String[] args) {
        /* YOUR CODE HERE */
    }
}