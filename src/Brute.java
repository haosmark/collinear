import java.util.Arrays;

public class Brute {
    public static void main (String args[]) {
        String fileName = args[0];
        In reader = new In(fileName);
        int totalPoints = reader.readInt();
        Point[] points;
        // read all points into an array of points
        try {
            points = new Point[totalPoints];
            for (int i = 0; i < totalPoints; i++) {
                points[i] = new Point(reader.readInt(), reader.readInt());
            }
        } finally {
            reader.close();
            reader = null;
        }
        Arrays.sort(points);

        // set canvas
        StdDraw.setCanvasSize(400, 400);
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        StdDraw.show();

        for (int i = 0; i < totalPoints; i++) {
            points[i].draw();
        }
        // iterate over points and find slopes
        for (int i = 0; i < totalPoints - 3; i++) {
            Point p = points[i];
            for (int j = i + 1; j < totalPoints - 2; j++) {
                Point q = points[j];
                for (int k = j + 1; k < totalPoints - 1; k++) {
                    Point r = points[k];
                    // compare slopes of pq and qr and enter 4th loop only if slopes are equal
                    if (p.slopeTo(q) == q.slopeTo(r)) {
                        for (int h = k + 1; h < totalPoints; h++) {
                            Point s = points[h];
                            if (q.slopeTo(r) == r.slopeTo(s)) { // if equal slopes, then success
                                System.out.println(p + " -> " + q + " -> " + r + " -> " + s);
                                p.drawTo(s);
                            }
                        }
                    }
                }
            }
        }
    }
}
