import org.junit.Test;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.number.IsCloseTo.closeTo;
import static org.hamcrest.number.OrderingComparison.lessThanOrEqualTo;

public class PointTest {
    @Test
    public void testEqualSlopes() {
        Point p = new Point(2, 3);
        Point q = new Point(3, 4);
        Point r = new Point(4, 5);

        double pqSlope = p.slopeTo(q);
        double qrSlope = q.slopeTo(r);
        assertThat(pqSlope, is(equalTo(qrSlope)));
    }
    @Test
    public void horizontalSlopeShouldReturnZero() {
        Point p = new Point(2, 3);
        Point q = new Point(3, 3);

        assertThat(p.slopeTo(q), is(equalTo(0.0)));
    }
    @Test
    public void verticalSlopeShouldReturnPositiveInfinity() {
        Point p = new Point(2, 3);
        Point q = new Point(2, 4);

        assertThat(p.slopeTo(q), is(Double.POSITIVE_INFINITY));
    }

    @Test
    public void verticalNegativeInfinity() {
        Point p = new Point(8, 4);
        Point q = new Point(8, 4);

        assertThat(p.slopeTo(q), is(Double.NEGATIVE_INFINITY));
    }

    @Test
    public void testCompareTo() {
        Point p = new Point(8, 4);
        Point q = new Point(8, 4);
        assertThat(p.compareTo(q), is(0));
        p = new Point(3, 9);
        q = new Point(8, 1);
        assertThat(p.compareTo(q), is(1));
        p = new Point(0, 0);
        q = new Point(2, 0);
        assertThat(p.compareTo(q), is(0));
    }

    @Test
    public void testSlopeValue() {
        Point p = new Point(1234, 5678);
        Point q = new Point(14000, 10000);

        assertThat(p.slopeTo(q), is(closeTo(0.33, 0.01)));
    }
    @Test
    public void testComparator() {
        String inputFile = "resources/input8.txt";
        In reader = new In(inputFile);
        int totalPoints = reader.readInt();
        Point[] points;
        // read all points into an array of points
        try {
            points = new Point[totalPoints];
            for (int i = 0; i < totalPoints; i++) {
                points[i] = new Point(reader.readInt(), reader.readInt());
            }
        } finally {
            reader.close();
            reader = null;
        }
        Point p = points[0];
        Arrays.sort(points, p.SLOPE_ORDER);
        for (int i = 1; i < totalPoints; i++) {
            assertThat(p.slopeTo(points[i - 1]), is(lessThanOrEqualTo(p.slopeTo(points[i]))));
        }
    }
}
